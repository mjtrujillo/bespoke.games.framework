#pragma once

#include "Game.h"
#include "Rectangle.h"

namespace Library
{
	class KeyboardComponent;
	class MouseComponent;	
}

namespace DirectX
{
	class SpriteFont;
}

namespace DragAndDropSprite
{
	class DragAndDropSpriteGame : public Library::Game
	{
	public:
		DragAndDropSpriteGame(std::function<void*()> getWindowCallback, std::function<void(SIZE&)> getRenderTargetSizeCallback);

		virtual void Initialize() override;
		virtual void Shutdown() override;
		virtual void Update(const Library::GameTime& gameTime) override;
		virtual void Draw(const Library::GameTime& gameTime) override;

	private:
		void Exit();

		static const DirectX::XMVECTORF32 BackgroundColor;
		static const DirectX::XMFLOAT2 HelpTextPosition;

		std::shared_ptr<Library::KeyboardComponent> mKeyboard;
		std::shared_ptr<Library::MouseComponent> mMouse;
		std::shared_ptr<DirectX::SpriteFont> mFont;

		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> mSpriteTexture;
		Library::Rectangle mSpriteBounds;
		bool mIsSpriteDragging;
		Library::Point mSpriteDraggingOffset;
	};
}