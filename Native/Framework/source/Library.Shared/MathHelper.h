#pragma once

namespace Library
{
	class MathHelper final
	{
	public:
		MathHelper() = delete;
		MathHelper(const MathHelper&) = delete;
		MathHelper& operator=(const MathHelper&) = delete;
		MathHelper(MathHelper&&) = delete;
		MathHelper& operator=(MathHelper&&) = delete;
		~MathHelper() = default;

		template<typename T>
		static constexpr const T& Clamp(const T& v, const T& lo, const T& hi)
		{
			return Clamp(v, lo, hi, std::less<>());
		}

		template<class T, typename Compare>
		static constexpr const T& Clamp(const T& v, const T& lo, const T& hi, Compare comp)
		{
			return assert(!comp(hi, lo)), comp(v, lo) ? lo : comp(hi, v) ? hi : v;
		}

		static float Lerp(float x, float y, float t)
		{
			return x + t * (y - x);
		}
	};
}